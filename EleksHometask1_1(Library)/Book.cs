﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace EleksHometask1_1_Library_
{
    class Book:IComparable
    {
        public List<Author> Author { get; set; }
        public string Title { get; set; }
        public int CountOfPages { get; set; }
        public Book()
        {
            Author = new List<Author>();
            Title = "";
            CountOfPages = 0;
        }
        public Book(List<Author> author,string title,int countOfPages) 
        {
            Author = author;
            Title = title;
            CountOfPages = countOfPages;
        }
        public override string ToString()
        {
            return Author + " " + Title + " " + CountOfPages;
        }

        public int CompareTo(object obj)
        {
            Book book = obj as Book;
            Debug.Assert(book != null, "book != null");
            return CountOfPages.CompareTo(book.CountOfPages);
        }
    }
}
