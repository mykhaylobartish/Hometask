﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace EleksHometask1_1_Library_
{
    class Department:IComparable,ICountingBooks
    {
        public List<Book> Books { get; set; }
        public string Name { get; set; }
        public Department()
        {
            Books = new List<Book>();
            Name = "";
        }
        public Department(List<Book> books,string name)
        {
            Books = books;
            Name = name;
        }
        public override string ToString()
        {
            return Name +" " + Books.Count;
        }
        public int CompareTo(object obj)
        {
            Department department = obj as Department;
            Debug.Assert(department != null, "department != null");
            return Books.Count.CompareTo(department.Books.Count);
        }

        public int Counting()
        {
            return Books.Count;
        }
    }
}
