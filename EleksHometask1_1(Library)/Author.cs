﻿using System;
using System.Diagnostics;

namespace EleksHometask1_1_Library_
{
    class Author:IComparable,ICountingBooks
    {
        public int CountOfBooks { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Author()
        {
            FirstName = "";
            LastName = "";
        }
        public Author(string firstName,string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        public override string ToString()
        {
            return (FirstName + " " + LastName);
        }

        public int CompareTo(object obj)
        {
            Author author = obj as Author;
            Debug.Assert(author != null, "author != null");
            return CountOfBooks.CompareTo(author.CountOfBooks);
        }

        public int Counting()
        {
            return CountOfBooks;
        }
    }
}
