﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace EleksHometask1_1_Library_
{
    static class OperationWithXmlDocuments
    {
        public static T FromXml<T>(string xml)
        {
            using (StringReader stringReader = new StringReader(xml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T) serializer.Deserialize(stringReader);
            }
        }

        public static string ToXml<T>(T obj)
        {
            using (StringWriter stringWriter = new StringWriter(new StringBuilder()))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                xmlSerializer.Serialize(stringWriter, obj);
                return stringWriter.ToString();
            }
        }

        public static void UpdateXml(string nodes,string xmlDoc,string newValue)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlDoc);
            XmlNodeList aNodes = doc.SelectNodes(nodes);
            if (aNodes != null)
                foreach (XmlNode aNode in aNodes)
                {
                    XmlAttribute attribute = aNode.Attributes?[""];
                    if (attribute != null)
                    {
                        string currentValue = attribute.Value;
                        if (string.IsNullOrEmpty(currentValue))
                        {
                            attribute.Value = newValue;
                        }
                    }
                }
        }

        public static void DeleteNodeFromXml(string path,string tagName,string attribute)
        {
            XmlDocument doc = new XmlDocument();

            doc.Load(path);

            XmlNodeList projectNodes = doc.GetElementsByTagName(tagName);

            for (int i = 0; i < projectNodes.Count; i++)
            {
                if (projectNodes[i].Attributes?[attribute] != null)
                {
                        doc.RemoveChild(projectNodes[i]);
                        doc.Save(path);
                }
            }
        }
    }
}
