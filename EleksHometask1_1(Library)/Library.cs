﻿using System.Collections.Generic;

namespace EleksHometask1_1_Library_
{
    class Library:ICountingBooks
    {
        public List<Department> Departments { get; set; }
        public string Name { get; set; }
        public Library()
        {
            Departments = new List<Department>();
            Name = "";
        }
        public Library(List<Department> departments,string name)
        {
            Departments = departments;
            Name = name;
        }

        public override string ToString()
        {
            return Name + " " + Departments.Count;
        }
        public int Counting()
        {
            int sum = 0;
            foreach (Department el in Departments)
                for (int index = 0; index < el.Books.Count; index++)
                {
                    sum++;
                }
            return sum;
        }

        public string AuthorWithMaxCountOfBooks()
        {
            return null;
        }

        public string DepartmentWithMaxCountOfBooks()
        {
            return null;
        }

        public string BookWithMinCountOfPages()
        {
            return null;
        }

    }
}
