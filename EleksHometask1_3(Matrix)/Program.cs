﻿using System;

namespace EleksHometask1_3_Matrix_
{
    class Program
    {
        static void Main()
        {
            Matrix matr = new Matrix();
            matr.SetMatrix();
            matr.GetMatrix();
            matr.SumWithNegative();
            matr.SaddlePoint();
            Console.ReadLine();
        }
    }
}
