﻿using System;
using System.Collections.Generic;

namespace EleksHometask1_3_Matrix_
{
    class Matrix
    {
        public int N, M;
        private int[,] _matrix;
        public void SetMatrix()
        {
            Console.Write("n :");
            N = Convert.ToInt32(Console.ReadLine());
            Console.Write("m :");
            M = Convert.ToInt32(Console.ReadLine());
            _matrix = new int[N, M];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    Console.Write("matrix[{0},{1}] = ", i, j);
                    _matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
        }
        public void GetMatrix()
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    Console.Write("{0} ", _matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
        public void SumWithNegative()
        {
            int sum = 0;
            bool check = false;
            List<int> sums = new List<int>();
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    sum += _matrix[i, j];
                    if (_matrix[i, j] < 0) check = true;
                }
                if (check) sums.Add(sum);
                sum = 0;
                check = false;
            }
            foreach (int el in sums)
                Console.Write("{0} ", el);
            Console.WriteLine();
        }
        public void SaddlePoint()
        {
            int counter1 = 0;
            int counter2 = 0;
            List<string> saddle = new List<string>();
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    for (int k = 0; k < M; k++)
                    {
                        if (_matrix[i, j] <= _matrix[i, k]) counter1++;
                    }
                    for (int k = 0; k < N; k++)
                    {
                        if (_matrix[i, j] >= _matrix[k, j]) counter2++;
                    }
                    if ((counter1 == M) && (counter2 == N))
                    {
                        var point = "[" + i + "," + j + "]";
                        saddle.Add(point);
                    }
                    counter1 = 0;
                    counter2 = 0;
                }
            }
            foreach (string el in saddle)
                Console.Write("{0} ", el);
        }
    }
}
