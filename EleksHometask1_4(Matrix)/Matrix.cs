﻿using System;
using System.Collections.Generic;

namespace EleksHometask1_4_Matrix_
{
    class Matrix
    {
        public int N, M;
        private int[,] _matrix;
        public void SetMatrix()
        {
            Console.Write("n :");
            N = Convert.ToInt32(Console.ReadLine());
            Console.Write("m :");
            M = Convert.ToInt32(Console.ReadLine());
            _matrix = new int[N, M];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    Console.Write("matrix[{0},{1}] = ", i, j);
                    _matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
        }
        public void GetMatrix()
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    Console.Write("{0} ", _matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
        public void SumWithNegative()
        {
            int sum = 0;
            bool check = false;
            List<int> sums = new List<int>();
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    sum += _matrix[i, j];
                    if (_matrix[i, j] < 0) check = true;
                }
                if (check) sums.Add(sum);
                sum = 0;
                check = false;
            }
            foreach (int el in sums)
                Console.Write("{0} ", el);
            Console.WriteLine();
        }
        public void KRowEqualKColumn()
        {
            int counter = 0;
            List<int> equals = new List<int>();
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    if (_matrix[i, j] == _matrix[j, i]) counter++;
                }
                if (counter == N) equals.Add(i);
                counter = 0;
            }
            foreach (int el in equals)
                Console.Write("{0} ", el);
        }
    }
}
