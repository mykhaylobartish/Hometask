﻿using System;

namespace EleksHometask1_2_Matrix_
{
    internal class Matrix
    {
        public int N, M;
        private int[,] _matrix;
        public void SetMatrix()
        {
            Console.Write("n :");
            N = Convert.ToInt32(Console.ReadLine());
            Console.Write("m :");
            M = Convert.ToInt32(Console.ReadLine());
            _matrix = new int[N, M];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                {
                    Console.Write("matrix[{0},{1}] = ", i, j);
                    _matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
        }
        public void GetMatrix()
        {
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    Console.Write("{0} ", _matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
        public int FirstRowWithoutNegative()
        {
            bool check = true;
            int rowNumber = -1;
            int a = 0;
            for (int i = 0; i < N; i++)
            {
                rowNumber++;
                for (int j = 0; j < M; j++)
                {
                    if (_matrix[i, j] >= 0) a++;
                    if (a == M) check = false;
                }
                if (check == false) break;
                a = 0;
            }
            return rowNumber;
        }
        public void AscendingEqualNumbers()
        {
            int[,] count=new int[2,N];
            for (int i = 0; i < N; i++)
                count[1, i] = i;
            int counter=1;
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M - 1; j++)
                {
                    for (int k = j + 1; k < M; k++)
                        if (_matrix[i, j] == _matrix[i, k]) counter++;
                    if (counter > count[0, i]) count[0, i] = counter;
                    counter = 1;
                }
            int[,] tempMatrix = new int[N, M];
            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                    tempMatrix[i,j] = _matrix[i,j];
            for (int i = 0; i < N-1; i++)
                for (int k = i + 1; k < N; k++)
                    if (count[0,i] > count[0,k])
                    {
                        var tempInt1 = count[0,k];var tempInt2 = count[1, k];
                        count[0,k] = count[0,i];count[1, k] = count[1, i];
                        count[0,i] = tempInt1;count[1, i] = tempInt2;
                    }
            
            for (int i = 0; i < N; i++)
                for(int j = 0; j < M; j++)
            {
                    _matrix[i, j] = tempMatrix[count[1, i], j];
            }
        }
    }
}
