﻿using System;

namespace EleksHometask1_2_Matrix_
{
    class Program
    {
        static void Main()
        {
            Matrix matr = new Matrix();
            matr.SetMatrix();
            matr.GetMatrix();
            Console.WriteLine(matr.FirstRowWithoutNegative());
            matr.AscendingEqualNumbers();
            matr.GetMatrix();
            Console.ReadLine();
        }
    }
}
